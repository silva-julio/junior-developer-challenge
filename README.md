# COSMO CHALLENGE

## Instructions

1. Do your best.
2. Be prepared to present the solution to the CosmoBots Developers.

_Get in touch if you need some help._

## Specification

### User Story

_As a front-end developer_
_I want an API where I can create, retrieve, update, delete, query by groups and associate users with pre-defined groups_
_So that I can integrate with the plataform add users form under the my account options._

### Implementation Details

#### User API Schema

```json
{
  "userId": uuid / v4,
  "accountId": uuid / v4,
  "firstName": string,
  "lastName": string,
  "email": email,
  "groupId": uuid / v4
}
```

#### Groups Table

| GROUP_ID | GROUP_NAME     | GROUP_DESCRIPTION |
| -------- | -------------- | ----------------- |
| UUID/v4  | Support Agents |                   |
| UUID/v4  | Bot Builders   |                   |
| UUID/v4  | Bot Admins     |                   |

## Requirements

- Should be implemented in Node.JS.
- Should make use of PostgreSQL as database.

## Recomendations

- Make use of good development practices: <https://12factor.net/>
- Tests are very important
- Use SOLID: <https://en.wikipedia.org/wiki/SOLID>
- Follow the REST Architecture: <https://restfulapi.net/>
- It's good to write nice git commit messages: <http://karma-runner.github.io/3.0/dev/git-commit-msg.html>
